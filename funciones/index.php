<?php
//Una funcion es un conjunto de instrucciones agrupadas bajo un nombre concreto 
// y que puede reutilizarse solamente cuando invocan a la función , tantas veces como 
// queramos
/*
 * function <nombre de la funcion>(parametros){
 *    conjunto de instrucciones
 * }
 * 
 */

function nombre(){
    echo "Mis nombres son Jhonatan Cesar";
}

function apellido(){
    echo " Mis apellidos son Almeida Vaca";
}


nombre()." ".apellido();

function nombresCompletos(){
    nombre()." ".apellido();
}
echo "<br>";

nombresCompletos();

echo "<br>";
function listaEstudiantes(){
    $html = "Anthony Jurado<br>";
    $html .= "Alberto Liberio<br>";
    $html .= "Jhon Chango";
    $html .= "Xiomara Macias";
    $html .= "Eneira Yepez";
    
    return $html;
}

echo listaEstudiantes();
echo "<br>";

function tabla($num){
    $tabla="La tabla del $num es <br>";
    for($i=0;$i<=10;$i++){
         $tabla .= "El resultado de $i x $num = ".$i*$num."<br>";
     }
     
     return $tabla;
}

echo tabla(2);
echo "<br>";
echo tabla(3);
echo "<br>";
echo tabla(4);
echo "<br>";
echo tabla(5);
echo "<br>";
echo tabla(6);
echo "<br>";
echo tabla(7);
echo "<br>";
echo tabla(8);
echo "<br>";
echo tabla(9);
