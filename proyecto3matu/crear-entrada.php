<?php
    include './includes/redireccion.php';
    include './includes/cabecera.php';
    include './includes/lateral.php';
?>
<div id="principal">
    <h1>Crear Entrada</h1>
    <p>Pantalla para crear una entrada</p>
    <hr><!-- comment -->
    <br><!-- comment -->
    <?php if(isset($_SESSION['correcto'])): ?>
            <div class="alerta alerta-exito">
                <?= $_SESSION['correcto']; ?>
            </div>  
    <?php elseif(isset($_SESSION['errores_entrada'])): ?>
            <div class="alerta alerta-error">
                <?= $_SESSION['errores_entrada']; ?>
            </div>  
    <?php endif; ?>
    <form action='guardar-entrada.php' method="POST">
        <label for="titulo">Titulo</label>
        <input type="text" name="titulo" id='titulo'>
        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'titulo') : ' ';  ?>       
        <label for="descripcion">Descripción</label>
        <textarea name="descripcion" style="height: 200px; width: 800px;"></textarea>
        <?php echo isset($_SESSION['errores']) ? mostrarError($_SESSION['errores'], 'descripcion') : ' ';  ?>
        <label for="categoria">Categoría</label>
        <select name="categoria">
            <?php
            $categorias = conseguirCategorias($db);
            if (!empty($categorias)):
                while($categoria = mysqli_fetch_assoc($categorias)):
            ?>
            <option value="<?=$categoria['id']?>"><?=$categoria['nombre']?></option>
            <?php
            endwhile;
            endif;
            ?>
        </select>
    
        <input type="submit" value="Guardar">
            
    </form>
    <?php            barrarErrores() ?>
</div>
<?php
require './includes/fooder.php';
?>
