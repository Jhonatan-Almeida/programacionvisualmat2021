<?php
    include './includes/redireccion.php';
    include './includes/cabecera.php';
    require './includes/lateral.php';
?>
            <div id="principal">
                <h3>Crear Categoría</h3>
                <p> En esta pantalla creamos la categoria correspondiente, para las entradas posibles </p>
                <br>
                <hr>
                <?php if(isset($_SESSION['completo_categoria'])): ?>
                    <div class="alerta alerta-exito">
                        <?= $_SESSION['completo_categoria']; ?>
                    </div>
                <?php elseif(isset($_SESSION['error_categoria'])): ?>
                    <div class="alerta alerta-error">
                        <?= $_SESSION['error_categoria']; ?>
                    </div>
                 <?php endif; ?>
                <form action="guardar_categoria.php" method="POST">
                    <label for="nombre">Nombre Categoría</label>
                    <input type="text" name="nombre">
                    <input type="submit" value="Guardar">
                    
                </form>
                <?php barrarErrores(); ?>
            </div>
<?php
            require_once './includes/fooder.php';
?>


