<?php
    include './includes/cabecera.php';
    require './includes/lateral.php';
?>
            
            <div id="principal">
                <h1>Entradas del BLOG</h1>
                <hr><!-- comment -->
                <?php
                    $entradas = conseguirEntradas($db,$limite = true);
                    if (!empty($entradas)):
                        while($entrada = mysqli_fetch_assoc($entradas)):
                ?>
                   <!--Aqui va ir todos los articulos de las entrdas mientras itere el while-->
                    <article class='entrada'>
                        <a href="entrada.php?id=<?= $entrada['id']?>">
                            <h2><?= $entrada['titulo']?></h2> 
                            <span class="fecha"><?=$entrada['nombre'].' | '.$entrada['fecha']?></span>
                            <p><?= substr($entrada['descripcion'],0,180)."...Leer mas"; ?></p>
                        </a>

                    </article>     
                <?php
                        endwhile;
                    endif;
                ?>
                
            </div>
<?php
            require_once './includes/fooder.php';
?>

