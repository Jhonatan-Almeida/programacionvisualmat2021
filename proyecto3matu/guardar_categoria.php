<?php
session_start();
if(isset($_POST)){
    // Conexión a la base de datos
    include './includes/conexion.php';
    // recoger lo datos de post
   $nombre = isset($_POST['nombre']) ? mysqli_real_escape_string($db,$_POST['nombre']):false;
   // validar datos
   $errores = array();
   
   if (!empty($nombre) && !is_numeric($nombre) && !preg_match("/[0-9]/", $nombre)){
        $nombre_valido = true;
    }else{
        $nombre_valido = false;
        $errores['nombre'] = "El nombre no es valido";
    }
    //insertar en la base de datos
    if (count($errores) == 0){
        $sql = "insert into categorias values(null,'$nombre')";
        $guardar = mysqli_query($db, $sql);
        if ($guardar){
           $_SESSION['completo_categoria'] = 'Se guardó correctamente la categoría';
        }else{
            $_SESSION['error_categoria'] = 'Error al guardar categoría';
        }
        
    }
}
header("Location: crear-categoria.php");