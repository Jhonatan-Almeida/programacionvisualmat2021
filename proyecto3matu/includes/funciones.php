<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function mostrarError($errores, $campo){
    $alerta="";
    if(isset($errores[$campo]) && !empty($campo)){
        $alerta ="<div class='alerta alerta-error'>".$errores[$campo]."</div>";
    }
    return $alerta;
}
function barrarErrores(){
    $borrado  = false;
    if (isset($_SESSION['errores'])){
        $_SESSION['errores'] = NULL;
        $borrado = TRUE;
    }
    if (isset($_SESSION['error_login'])){
        $_SESSION['error_login'] = null;
        $borrado = TRUE;
    }
    if (isset($_SESSION['errores']['general'])){
        $_SESSION['errores']['general'] = NULL;
        $borrado = TRUE;
    }
    if (isset($_SESSION['completado'])){
        $_SESSION['completado'] = NULL;
        $borrado = TRUE;
    }
    if (isset($_SESSION['completo_categoria'])){
        $_SESSION['completo_categoria'] = NULL;
        $borrado = TRUE;
    }
    if (isset($_SESSION['error_categoria'])){
        $_SESSION['error_categoria'] = NULL;
        $borrado = TRUE;
    }
    if (isset($_SESSION['correcto'])){
        $_SESSION['correcto'] = NULL;
        $borrado = TRUE;
    }
    if (isset($_SESSION['errores_entrada'])){
        $_SESSION['errores_entrada'] = NULL;
        $borrado = TRUE;
    }
    return $borrado;
}
function conseguirCategorias($conexion){
    $sql = "select * from categorias order by id asc";
    $categorias = mysqli_query($conexion, $sql);
    $resultado = array();
    if($categorias && mysqli_num_rows($categorias) >= 1){
        $resultado = $categorias;
    }
    return $resultado;
}

function conseguirCategoria($conexion, $id){
    $sql = "select * from categorias where id= $id";
    $categorias = mysqli_query($conexion, $sql);
    $resultado = array();
    if($categorias && mysqli_num_rows($categorias) >= 1){
        $resultado = mysqli_fetch_assoc($categorias);
    }
    return $resultado;
}

function conseguirEntradas($conexion, $limite = null,$categoria = null){
    $sql = "select e.*, c.nombre from entradas e
            inner join categorias c on c.id = e.categoria_id";
    if(!empty($categoria)){
        $sql .=" where e.categoria_id = $categoria";
    }
    if($limite){
        $sql .= " limit 3";
    }
    
    $entradas = mysqli_query($conexion, $sql);
    $resultado = array();
    if($entradas && mysqli_num_rows($entradas) >= 1){
        $resultado = $entradas;
    }
    return $resultado;
}

function conseguirEntrada($conexion, $id){
    $sql = "select e.*, c.nombre as categoria, 	CONCAT(u.nombre,' ',u.apellidos) 
            as usuario from entradas e
            inner join categorias c on e.categoria_id = c.id
            inner join usuarios u on e.usuario_id = u.id
            where  e.id = $id";
    $entradas = mysqli_query($conexion, $sql);
    $resultado = array();
    if($entradas && mysqli_num_rows($entradas) >= 1){
        $resultado = mysqli_fetch_assoc($entradas);
    }
    return $resultado;
}