<?php 
  include './includes/conexion.php';
  include './includes/funciones.php';
  $entrada_actual = conseguirEntrada($db, $_GET['id']);
  //print_r($entrada_actual); die();
  if(!isset($entrada_actual['id'])){
      header("Location: index.php");
  }

?>
<?php require_once './includes/cabecera.php'; ?>
<?php require_once './includes/lateral.php'; ?>
<div id="principal">
    <h1><?= $entrada_actual['titulo']; ?></h1>
    <a href="categoria.php?id=<?= $entrada_actual['categoria_id']; ?>">
        <h2><?= $entrada_actual['categoria']; ?></h2>
    </a>
    <h4><?= $entrada_actual['fecha'] ?> | <?= $entrada_actual['usuario'] ?></h4>
    <p>
        <?= $entrada_actual['descripcion'] ?>
    </p>
    <?php 
    if(isset($_SESSION["usuario"]) && isset($_SESSION["usuario"]["id"])== $entrada_actual['usuario_id']):
    ?>
    <a href="editar-entrada.php?id=<?= $entrada_actual['id'] ?>" class="boton boton-verde">Editar Entrada</a>
    <a href="eliminar-entrada.php?id=<?= $entrada_actual['id'] ?>" class="boton boton-rojo">Eliminar Entrada</a>
    <?php    endif; ?>
</div>
<?php
            require_once './includes/fooder.php';
?>