<?php

/* 
 Un bucle o ciclo, en programación, es una secuencia de instrucciones de código que se ejecuta repetidas veces, 
 * hasta que la condición asignada a dicho bucle deja de cumplirse.
// existen tres bucles imporatantes 
 * 
 * while(condicion){
 *      secuencia de instrucciones
 * }
 * for($i=0;$i<=10;$i++){
 *     Secuencia de instrucciones 
 * }
 * 
 * INCREMENTALES y DECREMENTALES
 * $nun++; $nun=$num+1;$num+1
 * $nun--; $nun=$num-1;$num-1
 * 
 * do{
 *      secuencia de instrucciones
 * }while(condicion)
 */
$num = $_GET[num];
echo "<h1>Tabla de multimplicar $num, mediante while</h1><br>";
if(isset($num)){
 $in= 0;
 while($in <= 10){
     echo "$in x $num = ".$in*$num."<br>";
     $in++;
 }
}
$numf = $_GET['numf'];
echo "<h1>Tabla de multimplicar $numf, mediante for</h1><br>";
if(isset($numf)){
 
    for($i=0;$i<=10;$i++){
         echo "$i x $numf = ".$i*$numf."<br>";
     }
}else{
    echo "Ingrese el número por get";
}
echo "<h1>Tabla de multimplicar $num, mediante do while</h1><br>";
if(isset($num)){
 $in= 0;
 do{
     echo "$in x $num = ".$in*$num."<br>";
     $in++;
 }while($in <= 10);
}