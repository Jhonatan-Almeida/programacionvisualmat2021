<?php
//El arreglo es un conjunto de datos utilizables como uno sola variable , contenedor de datos

// $arreglo = array(elementos);
// $arreglo = [];
//                    0        1        2         3
$zapatillas = array('Nike','Jordan','Reebook', 'adidas');

var_dump($zapatillas);
echo "<br>";
echo "<br>";
print_r ($zapatillas);
echo "<br>";
echo "<br>";
echo $zapatillas['2']."<br>";

//foreach ($zapatillas as $key => $value) {
//    
//}
echo "<ul>";
foreach ($zapatillas as $zapatilla) {
    echo "<li>".$zapatilla."</li>";
}
echo "</ul>";
//echo "<center>$zapatillas[1]</center>";

// arreglo con idetificadores 
$personas = [
    array(
        'nombre'=>'Jhoel Veliz',
        'direccion'=>'Guayacanes',
        'sexo'=>'Masculino',
        'identificacion'=>'0952546891'
    ),
    array(
        'nombre'=>'Bruno Cordero',
        'direccion'=>'Salitre',
        'sexo'=>'Masculino',
        'identificacion'=>'0952546892'
    ),
    array(
        'nombre'=>'Britney Cabrera',
        'direccion'=>'Limonal',
        'sexo'=>'Femenino',
        'identificacion'=>'0952546893'
    ),
    array(
        'nombre'=>'Maria Cedeño',
        'direccion'=>'Casuarina',
        'sexo'=>'Femenico',
        'identificacion'=>'0952546894'
    ),
    array(
        'nombre'=>'Jhonatan Almeida',
        'direccion'=>'Guaranda',
        'sexo'=>'Masculino',
        'identificacion'=>'1712158975'
    ),
    array(
        'nombre'=>'Nayeli Anastacio',
        'direccion'=>'Guayaquil',
        'sexo'=>'Femenino',
        'identificacion'=>'1712158989'
    )
];

//var_dump($persona);

$tabla = "<table border='2'>
            <tr>
                <td>Nombres</td>
                <td>Dirección</td>
                <td>Sexo</td>
                <td>identificacion</td>
            </tr>";
foreach ($personas as $key => $persona) {
         $tabla .= "<tr>
                <td>".$persona['nombre']."</td>
                <td>".$persona['direccion']."</td>
                <td>".$persona['sexo']."</td>
                <td>".$persona['identificacion']."</td>
            </tr>";
                 
}
            
        $tabla .= "</table>";



echo $tabla;

//<tr>
//                <td>".$persona[0]['nombre']."</td>
//                <td>".$persona[0]['direccion']."</td>
//                <td>".$persona[0]['sexo']."</td>
//                <td>".$persona[0]['identificacion']."</td>
//            </tr>
//            <tr>
//                <td>".$persona[1]['nombre']['firstname']." ".$persona[1]['nombre']['lastname']."</td>
//                <td>".$persona[1]['direccion']."</td>
//                <td>".$persona[1]['sexo']."</td>
//                <td>".$persona[1]['identificacion']."</td>
//            </tr>
//            <tr>
//                <td>".$persona[2]['nombre']."</td>
//                <td>".$persona[2]['direccion']."</td>
//                <td>".$persona[2]['sexo']."</td>
//                <td>".$persona[2]['identificacion']."</td>
//            </tr>
//            <tr>
//                <td>".$persona[3]['nombre']."</td>
//                <td>".$persona[3]['direccion']."</td>
//                <td>".$persona[3]['sexo']."</td>
//                <td>".$persona[3]['identificacion']."</td>
//            </tr>